
let dataTypeMenu = document.getElementsByName("dataTypeMenu");
let ownDataForm = document.getElementById("ownDataForm");
let ownDataArea = document.getElementById("ownDataArea");
let resultText = document.getElementById("result");
let generateButton = document.getElementById("generateButton");
let quantity = document.getElementById("quantity");
let selectedDataType = "celticmale";

// Selection of input data
dataTypeMenu.forEach(x => x.addEventListener("click", function() {
        selectedDataType = x.value;
        ownDataForm.style.display = x.value === "other" ? "block" : "none";
    }
));

// Generate button
generateButton.addEventListener("click", function() {
    let inputData = getInputData(selectedDataType);

    if (inputData.length < 5) {
        window.alert("Vlož alespoň pět různých řádků.");
        return;
    }        

    let names = generateNames(inputData, quantity.value);
    resultHeader.style = "block";
    resultText.style = "width:100%";
    resultText.innerHTML = getTableFromNames(names);
})

function getTableFromNames(names) {
    const colPerRow = 5;
    let result = "";
    let rows = names.length / colPerRow;

    for (var row = 0; row < rows; row++) {
        result += "<tr>"
        for (var col = 0; col < colPerRow; col++) {
            let index = row * colPerRow + col;
            if (index >= names.length)
                break;
            result += "<td>" + names[index] + "</td>"; 
        }
        result += "</tr>"
    }

    return result;
}

function getInputData(inputName) {    
    switch (inputName)
    {
        case "celticmale": return celticmale;
        case "celticwomen": return celticwomen;
        case "slovmale": return slovmale;
        case "slovwomen": return slovwomen;
        case "engmen": return engmen;
        case "engwomen": return engwomen;
        case "asia": return asia;
        case "dumbs": return dumbs;
        case "countries": return countries;
        case "daemon": return daemon;
        case "wizard": return wizard;

        case "other": return ownDataArea.value.split('\n');
    }
}

function generateNames(inputData, resultNumber) {
    let result = [];    
    for (var i = 0; i < resultNumber; i++) {
        result.push(generateName(inputData, inputData));
    }

    return result;
}

function getSubstringArray(input, startIndex, length){
    let resultArray = [];
    input.forEach(x => resultArray.push(x.substr(startIndex, length)));
    return resultArray;
}

function getSubstringArrayEnd(input, indexFromEnd, length){
    let resultArray = [];
    input.forEach(x => resultArray.push(x.substr(x.length - indexFromEnd, length)));
    return resultArray;
}

function generateName(inputData, endingData){
    let newName = "";
    let randomLengthIndex = Math.trunc(Math.random() * inputData.length);
    let newLength = inputData[randomLengthIndex].length;
    if (newLength == 0)
        return newName;
    let startStringLength = 1;
    let substringLength = Math.trunc(Math.max(newLength / 3, 1));
    let endStringLength = Math.trunc(Math.max(newLength / 2, 1));
    let data = [];

    while (newName.length < newLength)
    {
        // First char
        if (newName.length == 0) {
            data = getSubstringArray(inputData, 0, startStringLength);
        }
        else {
            index = newName.length;

            // End string
            if (newName.length + endStringLength >= newLength)
            {
                data = endingData.filter(x => x.length > endStringLength && x[x.Length - endStringLength - 1] == newName[index - 1]);

                // If we can´t find valid data, we must try something
                if (data.length == 0)
                    data = endingData.filter(x => x.length > endStringLength);

                data = getSubstringArrayEnd(data, endStringLength, endStringLength);
            }
            else
            {
                // Inner string
                data = inputData.filter(x => x.length >= index + substringLength && x[index - 1] == newName[index - 1]);

                if (data.length == 0)
                    data = inputData.filter(x => x.length >= index + substringLength);

                data = getSubstringArray(data, index, substringLength);
            }

            if (data.length == 0) return newName;
        }

        index = Math.trunc(Math.random() * data.length);
        newName += data[index];
    }

    return newName;
}